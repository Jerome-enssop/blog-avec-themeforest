<?php

include "config.php";

$recupTitre = isset($_POST['titre']) && !empty($_POST['titre']) ? $_POST['titre']: "";
$recupAnnee = isset($_POST['annee']) && !empty($_POST['annee']) ? $_POST['annee']: "";
$recupDescription = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description']: "";
$recupRealisateur = isset($_POST['realisateur']) && !empty($_POST['realisateur']) ? $_POST['realisateur']: "";
$recupDuree = isset($_POST['duree']) && !empty($_POST['duree']) ? $_POST['duree']: "";
$recupPhoto = isset($_FILES["photo"]) ? "images/".microtime().$_FILES["photo"]["name"]: "";

if (isset($_POST['submit'])){
	if (isset($_POST['titre']) && !empty($_POST['titre']) 
    && isset($_POST['annee']) && !empty($_POST['annee'])  
    && isset($_POST['description']) && !empty($_POST['description']) 
	&& isset($_POST['realisateur']) && !empty($_POST['realisateur']) 
	&& isset($_POST['duree']) && !empty($_POST['duree'])  
    && isset($_FILES['photo']) && !empty($_FILES['photo']) 
    )   {
		$req = $bdd->prepare("INSERT INTO film (titre, annee, descriptions, realisateur, duree, photo) VALUES (?,?,?,?,?,?)"); 
		$req->execute([$recupTitre, $recupAnnee, $recupDescription, $recupRealisateur, $recupDuree, $recupPhoto]);
		move_uploaded_file($_FILES["photo"]["tmp_name"], $recupPhoto);
		header("Location: index.php");
	}
}

include "nav.html";

?>
<body>

<div class="loader"><div class="loader_html"></div></div>

<div id="wrap" class="grid_1200">
	
	<header id="header">
		<section class="container clearfix">
			<div class="logo"><a href="index.php"><img alt="" src="images/enssop_small.png"></a></div>
			<nav class="navigation">
				<ul>
					<li class="current_page_item"><a href="index.php">Home</a></li>
					<li class="ask_question"><a href="#ancre-ajout">Ajouter un film</a></li>
				</ul>
			</nav>
		</section><!-- End container -->
	</header><!-- End header -->
	
	<section class="container main-content">
		<div class="row">
			<div class="col-md-9">				
				<div class="tabs-warp question-tab">
		            <div class="tab-inner-warp">
						<div class="tab-inner">
						<?php
						$req = $bdd->prepare("SELECT * FROM film ORDER BY titre"); 
						$req->execute();
						$results = $req->fetchAll();
						foreach($results as $film) { ?>
							<article class="post clearfix">
								<div class="post-inner">
									<div class="post-img"><a href="single_post_full_width.php?id=<?php echo $film["id_film"] ?>"><img src="<?php echo $film['photo'] ?>" alt=""></a></div>
									<div class="post-content">
										<p><?php echo $film['descriptions']; ?></p>
										<a href="single_post_full_width.php?id=<?php echo $film["id_film"] ?>" class="post-read-more button color small">Continuer la lecture</a>
									</div><!-- End post-content -->
								</div><!-- End post-inner -->
							</article><!-- End article.post -->
						<?php } ?>
					    </div>
					</div>
		        </div><!-- End page-content -->
			</div><!-- End main -->
		</div><!-- End row -->
	</section><!-- End container -->

	<footer id="footer">
		<section class="container" id="ancre-ajout">
			<div class="row">
				<div id="respond" class="comment-respond page-content clearfix">
				    <div class="boxedtitle page-title"><h2>Ajouter un film</h2></div>
				    <form action="" method="post" id="commentform" class="comment-form" enctype="multipart/form-data">
				        <div id="respond-inputs" class="clearfix">
				            <p>
				                <label class="required" for="comment_name">Titre<span>*</span></label>
				                <input name="titre" type="text" value="" id="comment_name" aria-required="true">
				            </p>
				            <p>
				                <label class="required" for="comment_email">Photo<span>*</span></label>
				                <input name="photo" type="file" value="" id="comment_email" aria-required="true">
				            </p>
				            <p class="last">
				                <label class="required" for="comment_url">Année<span>*</span></label>
				                <input name="annee" type="number" value="" id="comment_url">
				            </p>
				        </div>
				        <div id="respond-textarea">
				            <p>
				                <label class="required" for="comment">Description<span>*</span></label>
				                <textarea id="comment" name="description" aria-required="true" cols="58" rows="10"></textarea>
				            </p>
						</div>
						<div id="respond-inputs" class="clearfix">
				            <p>
				                <label class="required" for="comment_name">Réalisateur<span>*</span></label>
				                <input name="realisateur" type="text" value="" id="comment_name" aria-required="true">
				            </p>
				            <p>
				                <label class="required" for="comment_email">Durée<span>*</span></label>
				                <input name="duree" type="number" value="" id="comment_email" aria-required="true">
				            </p>
				        </div>
				        <p class="form-submit">
				        	<input name="submit" type="submit" id="submit" value="Post Article" class="button small color">
				        </p>
				    </form>
				</div>
			</div><!-- End row -->
		</section><!-- End container -->
	</footer><!-- End footer -->
	
<?php

include "footer.html";

?>