<?php

include "config.php";

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupCommentaire = isset($_POST['commentaire']) && !empty($_POST['commentaire']) ? $_POST['commentaire']: "";

$recupIdFilm = isset($_GET["id"])?$_GET["id"] : "";

if (isset($_POST['submit'])){
	if (isset($_POST['nom']) && !empty($_POST['nom']) 
    && isset($_POST['commentaire']) && !empty($_POST['commentaire'])  
    )   {
		$req = $bdd->prepare("INSERT INTO commentaire (nom, commentaire, id_film, date_creation) VALUES (?,?,?,NOW())"); 
		$req->execute([$recupNom, $recupCommentaire, $recupIdFilm]);
		header("Location: single_post_full_width.php?id=$recupIdFilm");
	}
}

$req = $bdd->prepare("SELECT * FROM film
                         WHERE id_film = ?
                         ");
    $req->execute([$recupIdFilm]);
    $results = $req->fetchALL();
	$stockInfos = $results[0];
	
	
$req = $bdd->prepare("SELECT * FROM commentaire WHERE id_film = ?");
$req->execute([$recupIdFilm]);
$nb = $req -> rowcount();



include "nav.html";

?>
<body>

<div class="loader"><div class="loader_html"></div></div>

<div id="wrap" class="grid_1200">
	
	<header id="header">
		<section class="container clearfix">
			<div class="logo"><a href="index.php"><img alt="" src="images/enssop_small.png"></a></div>
			<nav class="navigation">
				<ul>
					<li class="current_page_item"><a href="index.php">Home</a></li>
				</ul>
			</nav>
		</section><!-- End container -->
	</header><!-- End header -->
	
	<section class="container main-content page-full-width">
		<div class="row">
			<div class="col-md-12">
				<article class="post single-post clearfix">
					<div class="post-inner">
						<div class="post-img"><a href="#"><img src="<?php echo $stockInfos['photo']?>" alt=""></a></div>
						<div><h1>Titre : <?php echo $stockInfos['titre']?></h2></div>
						<div><h2>Année : <?php echo $stockInfos['annee']?></h2></div>
						<div><h2>Réalisateur : <?php echo $stockInfos['realisateur']?></h2></div>
						<div><h2>Durée : <?php echo $stockInfos['duree']?> min</h2></div>
				        <div class="post-content">
				            <p><?php echo $stockInfos['descriptions']?></p>
				        </div><!-- End post-content -->
				    </div><!-- End post-inner -->
				</article><!-- End article.post -->
								
				<div id="commentlist" class="page-content">
					<div class="boxedtitle page-title"><h2>Commentaire(s) ( <span class="color"><?php echo $nb;?></span> )</h2></div>
					<?php
						$req = $bdd->prepare("SELECT * FROM commentaire WHERE id_film = ? ORDER BY date_creation DESC"); 
						$req->execute([$recupIdFilm]);
						$results = $req->fetchAll();
						foreach($results as $commentaire) { ?>
						<ol class="commentlist clearfix">
							<li class="comment">
								<div class="comment-body clearfix"> 
									<div class="comment-text">
										<h3><?php echo $commentaire['nom']?></h3>
										<h5><?php echo $commentaire['date_creation']?></h5>
										<div class="text"><p><?php echo $commentaire['commentaire']?></p></div>
									</div>
								</div>
							</li>
						</ol><!-- End commentlist -->
					<?php } ?>
				</div><!-- End page-content -->
				
				<div id="respond" class="comment-respond page-content clearfix">
				    <div class="boxedtitle page-title"><h2>Laisser un commentaire</h2></div>
				    <form action="" method="post" id="commentform" class="comment-form">
				        <div id="respond-inputs" class="clearfix">
				            <p>
				                <label class="required" for="comment_name">Nom<span>*</span></label>
				                <input name="nom" type="text" value="" id="comment_name" aria-required="true">
				            </p>
				        </div>
				        <div id="respond-textarea">
				            <p>
				                <label class="required" for="comment">Commentaire<span>*</span></label>
				                <textarea id="comment" name="commentaire" aria-required="true" cols="58" rows="10"></textarea>
				            </p>
				        </div>
				        <p class="form-submit">
				        	<input name="submit" type="submit" id="submit" value="Post Commentaire" class="button small color">
				        </p>
				    </form>
				</div>
				
			</div><!-- End main -->
		</div><!-- End row -->
	</section><!-- End container -->
	
<?php

include "footer.html";

?>

</body>
</html>